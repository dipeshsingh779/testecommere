from django.urls import path
from .views import *
urlpatterns = [
    path('', CustomerLoginView.as_view(), name='login'),
    path('cust/profile/', CustomerProfile.as_view(), name='profile'),
    path('cust/logout/', LogOutView.as_view(), name='logout'),
    path('cust/update/', ProfileUpdate.as_view(), name='update'),
    path('cust/ajax/update/', ProfileAjaxUpdate, name='ajaxupdate'),
]