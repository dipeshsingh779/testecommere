import json
import requests
from django.views.generic import *
from requests.sessions import Request, session
from .forms import LoginForm
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse


class CustomerLoginView(FormView):
    template_name = 'customertemplates/customerlogin.html'
    form_class = LoginForm

    def form_valid(self, form):
        parameters = {
        'email' : str(form.cleaned_data['username']),
        'password' : str(form.cleaned_data['password'])
        }
        #  Param from LOGIN form posted to API, if token response, means user is authenticated and active
        headers = {"Content-Type": 'application/json'}
        response = requests.post("http://localhost:8000/api/customer/login/", json=parameters, headers=headers)
        if response.status_code == 200:
            data = response.json()
            self.request.session['token'] = data['access']
            self.request.session['refresh'] = data['refresh']
            return redirect('profile')
        else:
            print('bad response')
            return HttpResponse('no, it not worked')


class CustomerProfile(View):
    def get(self, request):
        if request.session.get('token') is None:
            return redirect('login')
        url = 'http://localhost:8000/api/customer/profile/'
        token = self.request.session.get('token')
        headers = {'Authorization' : f'Bearer {token}'}
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            context = {
                'data' : response.json()
            }
            return render(request, 'customertemplates/customerprofile.html', context)
        else:
            print('wrong')
            context = {
                'status' : 'failure'
            }
            return render(request, 'customertemplates/customerprofile.html', context)


class LogOutView(View):
    def get(self, request):
        access_token = request.session['token']
        parameters = {
        'refresh_token' : request.session['refresh'],
        }
        headers = {'Authorization' : f'Bearer {access_token}',
                                    "Content-Type": 'application/json'}
        response = requests.post("http://localhost:8000/api/logout/", json=parameters, headers=headers)
        if response.status_code == 205:
            del request.session['token']
            del request.session['refresh']
            return redirect('login')
        else:
            print('no token')
            return redirect('login')


class ProfileUpdate(TemplateView):
    template_name = 'customertemplates/profileupdate.html'
    # def get(self, request):
    #     cust = self.request.POST.get('cust')
    #     print(cust)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        url = 'http://localhost:8000/api/customer/profile/'
        token = self.request.session.get('token')
        print(token)
        headers = {'Authorization' : f'Bearer {token}'}
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            data = response.json()
            context['name'] = data['message']['name']
            context['image'] = data['message']['image']
            context['mobile'] = data['message']['mobile']
            context['street_address'] = data['message']['street_address']
        return context

# class ProfileAjaxUpdate(View):
#     def post(self, request):
#         # import pdb;
#         # pdb.set_trace()
#         name = self.request.POST.get('cust')
#         mobile = self.request.POST.get('mobile')
#         street_address = self.request.POST.get('street_address')
#         print(name, mobile, street_address)
#         # image = self.request.FILES['image']
#         access_token = request.session['token']
#         parameters = {
#         'name' : name,
#         'mobile' : mobile,
#         'street_address' : street_address,
#         }
#         #  Param from LOGIN form posted to API, if token response, means user is authenticated and active
#         headers = {'Authorization' : f'Bearer {access_token}',
#                     "Content-Type": 'application/json'}
#         response = requests.patch("http://localhost:8000/api/customer/update/", json=parameters, headers=headers)
#         if response.status_code == 200:
#             return redirect('profile')
#             return JsonResponse({'status':'success'})
#         else:
#             return HttpResponse("didn't work")
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
from django.core.serializers.json import DjangoJSONEncoder
from django.core.files.uploadedfile import InMemoryUploadedFile
import os

class MyJsonEncoder(DjangoJSONEncoder):
    def default(self, o):
        if isinstance(o, InMemoryUploadedFile):
           return o.read()
        return str(o)
@csrf_exempt
def ProfileAjaxUpdate(request):
    import pdb;
    # pdb.set_trace()
    name = request.POST['cust']
    mobile = request.POST['mobile']
    street_address = request.POST['street_address']
    file1=request.FILES['image']
    access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjM0NDQ0NjQ1LCJqdGkiOiIxMGRmODgwMWU2NWI0YmU3OTdjNDI0NDE5YzMwMTQ3NCIsInVzZXJfaWQiOjR9.2ASHE1N2WLOH6Ls6hDCwPAA9v_nReNTGOd2tojTxEMY"
    print(access_token)
    # fs=FileSystemStorage()
    # payload = {'doc_file': file1.read()}
    # msg = json.dumps(payload, cls=MyJsonEncoder)
    # file_1_path=fs.save(file1.name,file1)
    # image_url = str(fs.url(file_1_path))
    # image_url = fs.url(file_1_path)
    # print(image_url, '_______')
    # print(type(image_url), '_______')
    # cwd = os.getcwd()
    # files = os.listdir(cwd)
    # print("Files in %r: %s" % (cwd, files))
    # a = files[6]
    # print(a)
    # with open(file1,'rb') as f:
    files = {'image' : file1}
    print(files)
    headers = {'Authorization' : f'Bearer {access_token}'}
    # data = fs.save(file1.name,file1)
    # print(data, '---------')
    body = {
        'name' : name,
        'mobile' : mobile,
        'street_address' : street_address,
        }
    # myfiles = {
    #     "image" : json.loads(data)
    #     }
    # print(myfiles)
    response = requests.patch("http://localhost:8000/api/customer/update/",json=body ,headers=headers)
    response = requests.patch("http://localhost:8000/api/customer/update/",files=files ,headers=headers)
    print(response.status_code)
    if response.status_code == 200:
        return JsonResponse({'status' : 'success'})
    else:
        return JsonResponse({'status' : 'failed'})